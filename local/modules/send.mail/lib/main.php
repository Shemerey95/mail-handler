<?php
namespace Send\Mail;

use Bitrix\Main\Diag\Debug;
use CIBlockElement;
use CModule;

CModule::IncludeModule('sale');
use Bitrix\Sale\Location;

class Main
{
    public function SendMail(&$arFields, &$arTemplate)
    {
        $url = $_SERVER['SERVER_NAME'];
        if (($chang = substr($url, 0, strpos($url, "."))) == 'xn--80aalwqglfe')
        {
            $arFields["MANAGER_EMAIL"] = 'ug-matras.krd@mail.ru';
        } elseif (($row = substr($url, 0, strpos($url, "."))) == 'xn--b1axaggg') {
            $arFields["MANAGER_EMAIL"] = 'ug-matras.rnd@mail.ru';
        } else {
            $arFields["MANAGER_EMAIL"] = 'ug-matras@mail.ru';
        }
    }
}
