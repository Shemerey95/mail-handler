<?php
IncludeModuleLangFile(__FILE__);
if (class_exists("send_mail")) {
    return;
}

use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;

class send_mail extends CModule
{
    
    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            include_once(__DIR__ . "/version.php");
            $this->MODULE_ID           = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION      = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME         = 'sendmail';
            $this->MODULE_DESCRIPTION  = 'Обработчик отправки писем о быстром и обычном заказе с сайта на разные имейлы';
            $this->PARTNER_NAME        = '';
            $this->PARTNER_URI         = '';
        }
    }
    
    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);
        $this->InstallEvents();
    }
    
    public function InstallEvents()
    {
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnBeforeEventSend",
            $this->MODULE_ID,
            "Send\Mail\Main",
            "SendMail"
        );
    }
    
    public function DoUninstall()
    {
        $this->UnInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);
    }
    
    public function UnInstallEvents()
    {
        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnBeforeEventSend",
            $this->MODULE_ID,
            "Send\Mail\Main",
            "SendMail"
        );
    }
}
